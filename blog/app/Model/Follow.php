<?php

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = 'flow';
    protected $fillable = ['id_people','id_peopleFlow'];
    public $timestamps = false;
    public function getUser(){
        
    }
    public function user(){
        return $this->belongsTo(User::class,'id_peopleFlow');
    }
}
