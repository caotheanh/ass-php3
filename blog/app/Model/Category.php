<?php

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Model\Posts;
class Category extends Model
{
    protected $table = 'categoris';
    protected  $fillable = ['user_id','name'];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function posts(){
        return $this->hasMany(Posts::class,'cate_id','id');
    }
}
