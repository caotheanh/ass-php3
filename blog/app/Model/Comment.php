<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Posts;
class Comment extends Model
{
    protected $table = 'comment';
    protected $fillable = ['post_id','user_id','content','is_active'];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function post(){
        return $this->belongsTo(Posts::class,'post_id');
    }
}
