<?php

namespace App\Model;

use App\User;
use App\Model\Category;
use App\Model\Comment;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title','content','cate_id','user_id','is_comment'];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function cate(){
        return $this->belongsTo(Category::class,'cate_id');
    }
    public function comment(){ 
        return $this->hasMany(Comment::class,'post_id');
    }
}
