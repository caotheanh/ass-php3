<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Model\Comment;
use App\Model\Posts;
use Illuminate\Support\Facades\Redirect;

class CommentController extends Controller
{
    public function comment()
    {
        $id_post = request()->post_id;
        $check = Posts::find($id_post);
        if ($check->is_comment==1 && Auth::check()) {
            $params = \Arr::except(request()->all(), ['_token']);
            $params['post_id'] = (int) request()->post_id;
            $params['user_id'] = (int) request()->user_id;
            return response()->json(Comment::create($params));
        }else{
            return response()->json(['messages'=>'Bài viết này đã bị chặn bình luận bởi Admin!']);
        };
    }
    public function deleteComment($id)
    {
        if (Comment::find($id)) {

            if (Comment::where('id', $id)->delete()) {
                return response()->json(['messages' => 'Xóa thành công!']);
            }
        } else {
            return response()->json(['messages' => 'Xóa thất bại!']);
        }
    }
    public function editComment($id)
    {
        if (Comment::find($id)) {

            return response()->json(Comment::find($id));
        }
    }
    public function updateComment()
    {
        $id = request()->idComment;
        $content = request()->contentComment;
        if (Comment::find($id)) {
            Comment::where('id', $id)->update(['content' => $content]);
            return back();
        }
    }
}
