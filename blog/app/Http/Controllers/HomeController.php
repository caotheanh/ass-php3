<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Comment;
use App\Model\Posts;
use App\User;
use Auth;
use App\Model\Follow;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function index()
    {
        // return response()->json('anh');
        $post  = Posts::with(['user', 'cate'])->orderBy('id', 'desc')->paginate(2);
        $cate = Category::all();
        return view('index', compact('post', 'cate'));
    }
    public function details($id)
    {
        $dataComment = Comment::with(['user', 'post'])->where('post_id', $id)->orderBy('id', 'desc')->paginate(2);
        $post  = Posts::with(['user', 'cate'])->where('id', $id)->get();
        return view('post', compact('post', 'dataComment'));
    }
    public function editPost($id)
    {
        if (Posts::find($id)) {
            return response()->json(Posts::find($id));
        }
    }
    public function savePost()
    {
        $id = request()->id;
        $checkPost = Posts::find($id);
        $data = request()->all();
        if ($checkPost) {
            return response()->json(Posts::where('id', $id)->update($data));
        }
    }
    public function postByCate($id)
    {
        $post = $post  = Posts::with(['user', 'cate'])->where('cate_id', $id)->orderBy('id', 'desc')->paginate(2);
        $cate = Category::all();
        return view('index', compact('post', 'cate'));
    }
    public function login()
    {
        if (request()->email && request()->password) {
            $result = Auth::attempt([
                'email' => request()->get('email'),
                'password' => request()->get('password')
            ]);
            if ($result && Auth::user()->is_active == 1) {
                return Redirect()->route('home');
            } else {
                Auth::logout();
                // return Redirect()->back();
                return Redirect()->route('home');
            }
        }else{
            // return Redirect()->back();
            return Redirect()->route('home');
        }
    }
    public function logout()
    {
        Auth::logout();
        // return Redirect()->back();
        return Redirect()->route('home');
    }
    public function changePassword()
    {
        $oldPassword = request()->oldPassword;
        $newPassword = request()->newPassword;
        $cfNewPassword = request()->cfNewPassword;
        $id = Auth::user()->id;
        $user =  User::find($id);
        if (Hash::check($oldPassword, $user->password)) {
            if ($newPassword !== $cfNewPassword) {
                return response()->json(['messages' => 'Xác nhận mật khẩu mới không trùng khớp!']);
            } else {
                $password = [
                    'password' => bcrypt($newPassword)
                ];
               $updateData= User::where('id', $id)->update($password);
                return response()->json($updateData);
            }
        } else {
            return response()->json(['messages' => 'Mật khẩu không đúng!']);
        }
    }
    public function profile()
    {
        if (Auth::check()) {
            $cate = Category::all();
            $id = Auth::user()->id;
            $data = Posts::with('user')->where('user_id', $id)->orderBy('id', 'desc')->paginate(2);
            return view('profile', compact('data', 'cate'));
        } else {
            return Redirect()->route('home');
        }
    }
    public function postProfile()
    {

        if (Auth::check()) {
            $id_user = Auth::user()->id;
            $params = \Arr::except(request()->all(), ['_token']);
            $params['title'] = request()->title;
            $params['content'] =  request()->content;
            $params['cate_id'] =  request()->cate_id;
            $params['user_id'] =  $id_user;
            if ($params['title'] != '' && $params['content'] !== '') {
                $data = Posts::create($params);
                if ($data) {
                    return Redirect()->route('profile');
                }
            } else {
                $err = 'Vui lòng điền đầy đủ!';
                return Redirect()->route('profile')->with(['messages' => $err]);
            }
        }
    }
    public function registerUser()
    {
        $data = [
            'name' => request()->rName,
            'DateOfBirth' => request()->rDate,
            'phone' => request()->rPhone,
            'email' => request()->rEmail,
            'password' => bcrypt(request()->rPassword),
        ];
        $checkEmail = User::where('email', $data['email'])->get();
        $checkPhone = User::where('phone', $data['phone'])->get();
        if (count($checkPhone) != 0) {
            return response()->json(['messages' => 'Số điện thoại đã tồn tại!']);
        } elseif (count($checkEmail) != 0) {
            return response()->json(['messages' => 'Email đã tồn tại!']);
        } elseif ($data['name'] && $data['DateOfBirth'] && $data['phone'] && $data['email'] && $data['password']) {
            return response()->json(User::create($data));
        } else {
            return response()->json(['messages' => 'Tạo mới thất bại!']);
        }
    }
    public function deleteProfilePost($id)
    {
        if (Posts::find($id)) {
            Comment::where('post_id', $id)->delete();
            Posts::where('id', $id)->delete();
            return Redirect()->route('profile');
        }
    }
    public function profile_follow($id)
    {
        $user = User::find($id);
        $check = Follow::where('id_peopleFlow', $id)->get();
        if (count($check) != 0) {
            $messages = 'Đang theo dõi';
        }else{
            $messages = '';
        }
        $data = Posts::with('user')->where('user_id', $id)->orderBy('id', 'desc')->paginate(2);
        if ($data) {
            return view('profile-follow', compact('data','messages','user'));
            // return response()->json($data);
        }
        // return view('profile-follow', compact('data'));
    }
    public function follow($id)
    {
        $check = Follow::where('id_peopleFlow', $id)->get();
        if (count($check) == 0) {
            if (Auth::user()) {
                $data = [
                    'id_people' => Auth::user()->id,
                    'id_peopleFlow' => request()->id_peopleFlow,
                ];
                $follower = Follow::create($data);
                if($follower){
                    $messages = 'Đã theo dõi';
                    return Redirect()->back()->with(['status' =>$messages]);
                }
            }
        }else{
            $messages = 'Đang theo dõi';
            return Redirect()->back()->with(['status' =>$messages]);
        }
    }
    public function unfollow($id){
        if(Auth::user()){
            $id_people = Auth::user()->id;
            Follow::where('id_people',$id_people)->where('id_peopleFlow',$id)->delete();
            return Redirect()->back();
        }
        
    }
    public function listFlw(){
        $id = Auth::user()->id;
            // dd($id);
        // if(Auth::user()){
            
            // $id = Auth::user()->id;
            $user = [];
           $data = Follow::where('id_people',$id)->get();
        //    dd(Auth::user()->id);
          for($i=0;$i<count($data);$i++){
            $user[] = Follow::with('user')->where('id_peopleFlow',$data[$i]->id_peopleFlow)->get();
          }
        //   return response()->json($user);
            return view('flw',compact('user'));
    }
}
