<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        return response()->json(User::all());
    }
    public function blockUser($id){
        if (User::find($id)->is_active==1) {
            return response()->json(User::where('id', $id)->update(['is_active' => 2]));
        }else{
            return response()->json(['messages'=>'Tài khoản này đã bị khóa!']);
        }
    }
}
