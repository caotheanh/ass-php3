<?php

namespace App\Http\Controllers;

use App\Model\Comment;
use Illuminate\Http\Request;

class AdminCommentController extends Controller
{
    public function getCount($id)
    {
        return response()->json(count(Comment::where('post_id', $id)->get()));
    }
    public function commentByPost($id)
    {
        return response()->json(Comment::with('user')->where('post_id', $id)->get());
    }
    public function activeComment($id)
    {
        if (Comment::find($id)->is_active==1) {
            return response()->json(Comment::where('id', $id)->update(['is_active' => 2]));
        }else{
            return response()->json(['messages'=>'Bình luận này đã bị ẩn trước đó!']);
        }
    }
    public function detailsComment($id){
        return response()->json(Comment::find($id));
    }
}
