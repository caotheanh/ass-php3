<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Posts;
use App\User;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::with('user')->get();
        return response()->json($data);
    }
    public function add()
    {
        $data = [
            'name' => request()->name,
            'user_id' => (int) request()->user_id
        ];
        $checkUser = User::find($data['user_id']);
        if ($checkUser) {
            return response()->json(Category::create($data));
        } else {
            return false;
        }
    }
    public function edit($id)
    {
        $check = Category::find($id);
        if ($check) {
            return response()->json($check);
        } else {
            return false;
        }
    }
    public function update()
    {
        $id = request()->id;
        $data = [
            'name' => request()->name,
        ];
        $check = Category::find($id);
        if ($check) {
            return response()->json(Category::where('id', $id)->update($data));
        } else {
            return false;
        }
    }
    public function destroy($id)
    {
        $check = Category::find($id);
        if ($check) {
            Category::where('id', $id)->delete();
            Posts::where('cate_id',$id)->delete();
            return response()->json('Xoa thanh cong');
        } else {
            return false;
        }
    }
}
