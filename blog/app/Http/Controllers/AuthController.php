<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use Hash;
class AuthController extends Controller
{   
    private $user;

    public function __construct(User $user){
        $this->user = $user;
    }
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        // $checkRole= User::where('email',$request->email);
        $checkRole= User::where('email',$request->email)->get();
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['invalid_email_or_password'], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        if($token && $checkRole[0]->role==2){
            return response()->json(compact('token'));
        }else{
            return response()->json(['messages'=>'Đăng nhập thất bại!']);
        }
        
    }

    public function getUserInfo(Request $request){
        JWTAuth::setToken($request->input('token'));
        $user = JWTAuth::toUser();
        return response()->json(['result' => $user]);
    }
}  