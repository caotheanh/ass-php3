<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Posts;
use App\Model\Category;
use App\Model\Comment;
use App\User;

class PostController extends Controller
{
    public function index()
    {
        $listPost  = Posts::with(['user','cate'])->get();
        return response()->json($listPost);
    }
    public function add()
    {
        $cate = Category::all();
        $request = request()->all();
        $userid = (int) request()->user_id;
        $cate_id = (int) request()->cate_id;
        $checkUser = User::find($userid);
        $checkCate = Category::find($cate_id);
        if ($checkUser && $checkCate) {
            return response()->json(Posts::create($request));
        } else {
            return false;
        }
        return response()->json($request);
    }
    public function edit($id)
    {
        $check = Posts::with('cate')->where('id',$id)->get();
        if ($check) {
            return response()->json($check);
        } else {
            return false;
        }
    }
    public function update()
    {
        $id = request()->id;
        $cate_id = request()->cate_id;
        $request = request()->all();
        $checkCate = Category::find($cate_id);
        $checkPost = Posts::find($id);
        if ($checkCate && $checkPost) {
            return response()->json(Posts::where('id', $id)->update($request));
        } else {
            return false;
        }
    }
    public function destroy($id)
    {
        $check = Posts::find($id);
        if ($check) {
            if(Comment::where('post_id',$id)->get()){
                Comment::where('post_id',$id)->delete();
            }
            
            return response()->json(Posts::where('id', $id)->delete());
        } else {
            return false;
        }
    }
    public function unActiveComment($id){ 
        if(Posts::find($id)){
            return response()->json(Posts::where('id',$id)->update(['is_comment'=>2]));
        }else{
            return response()->json(['messages'=>'Thất bại!']);
        }
    }
    public function activeComment($id){
        if(Posts::find($id)){
            return response()->json(Posts::where('id',$id)->update(['is_comment'=>1]));
        }else{
            return response()->json(['messages'=>'Thất bại!']);
        }
    }
}
