<?php

namespace App;

use App\Model\Category;
use App\Model\Posts;
use App\Model\Comment;
use App\Model\Follow;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'DateOfBirth', 'phone','email','password','role','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function posts(){
        return $this->hasMany(Posts::class,'user_id','id');
    }
    public function categoris(){
        return $this->hasMany(Category::class,'user_id','id');
    }
    public function comment(){
        return $this->hasMany(Comment::class,'user_id','id');
    }
    public function flw(){
        return $this->hasMany(Follow::class,'id_peopleFlow','id');
    }
}
