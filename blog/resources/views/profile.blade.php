@extends('master') @section('content')
<div class="container">
    @if(session('messages'))
    <p style="color: red;font-size: 16px">{{ session('messages') }}</p>
    @endif
    <form action="{{ route('profile-post') }}" method="POST">
        @csrf
        <textarea
            style="width: 100%;"
            rows="5"
            name="content"
            placeholder="Đăng bài viết mới"
        ></textarea>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Danh mục</label>
                    <select
                        class="form-control"
                        name="cate_id"
                        id="exampleFormControlSelect1"
                    >
                        @foreach ($cate as $cate)
                        <option
                            value="{{ $cate->id }}"
                            >{{ $cate->name }}</option
                        >
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Tiêu đề</label>
                   
                    <input type="text" name="title" class="form-control" />
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Đăng</button>
    </form>
    @if (count($data)>0)

    <div>
        @foreach ($data as $item)

        <div class="row">
            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                <div class="post-preview">
                    <a href="{{ route('post-details',['id'=>$item->id]) }}">
                        <h2 class="post-title">
                            {{ $item->title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ substr($item->content,0,30).'....' }}
                        </h3>
                    </a>
                    <p class="post-meta">
                        Đăng bởi
                        <a href="#">{{ $item->user->name }}</a>
                        lúc {{ $item->created_at }}
                    </p>
                </div>
            </div>

            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                <form
                    action="{{ route('deleteProfilePost',['id'=>$item->id]) }}"
                    method="POST">
                    @csrf
                    <button
                        style="background: rgb(128, 99, 235); color: white;"
                        type="submit">
                        Xóa
                    </button>
                </form>
                <button data-toggle="modal" data-target="#updatePost" style="width: 100px; height: 30px;font-size: 12px;background: rgb(241, 225, 225);border:1px solid black" type="button"onclick="edit({{ $item->id }})" >Chỉnh sửa</button>
            </div>

            <hr />
        </div>

        @endforeach
        <div class="modal fade" id="updatePost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa bài viết</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <input type="hidden" name='id' id="id">
                      <label for="title" class="col-form-label">Tiêu đề:</label>
                      <input class="form-control" id="title" name="title" />
                      <label for="content" class="col-form-label">Nội dung:</label>
                      <textarea class="form-control" id="content" name="content"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                    <button onclick="update()"  class="btn btn-primary">Chỉnh sửa</button>
                </div>
              {{-- </form> --}}
              </div>
            </div>
          </div>
        {{ $data->links() }}
    </div>

    @else
    <p>Bạn chưa có bài viết nào !</p>
    @endif
</div>
<script>
    edit = async id =>{
        const data = await axios.get(`http://localhost:8000/editPost/${id}`);
        document.getElementById('title').value = data.data.title;
        document.getElementById('id').value = data.data.id;
        document.getElementById('content').innerHTML = data.data.content;
    }
    update = async () =>{
       let title= document.getElementById('title').value; 
       let id= document.getElementById('id').value;
       let content= document.getElementById('content').value;
        const data = await axios.post(`http://localhost:8000/savePost`,{
            title,id,content
        });
        if(data.data){
            Swal.fire({
          icon: 'success',
          title: 'Chỉnh sửa bài viết thành công!',
          showConfirmButton: false,
          timer: 1500
        })
        setTimeout(function(){
          window.location.reload();
        },1000)
    }
        
        
    }
</script>
@endsection
