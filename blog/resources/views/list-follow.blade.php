
@extends('master')
@section('content')
<div class="container">
 <div class="row">
   <div class="col-lg-3">
     <h3>Danh sách người theo dõi</h3>
   </div>
   <div class="col-lg-12 col-md-12">
     @foreach ($follow as $item)
     <div class="post-preview">
       <a href="{{ route('profile-follow',['id'=>$item->id]) }}">
         <h2 class="post-title">
           {{ $item->name }}
         </h2>
       <p class="post-meta">Đăng bởi
         <a href="#">{{ $item->user->name }}</a></p>
     </div>
     <hr>
     @endforeach
     <div class="clearfix">
       {{ $post->links() }}
     </div>
   </div>
 </div>
</div>

<hr>
@endsection



