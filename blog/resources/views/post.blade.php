@extends('master')
@section('content')
<article>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <h2>{{ $post[0]->title }}</h2>
        <p>{{ $post[0]->content }}</p>
       <p style="margin-left: 50%;font-size:14px">
        <i>Đăng bởi <b>{{ $post[0]->user->name }}</b> lúc {{ $post[0]->created_at }}</i>
       </p>
       <hr>
       @if (Auth::user())
       <div id="formComment">
        <input type="hidden" name="user_id" id="user_id" value={{ Auth::user()->id }}>
        <input type="hidden" name="post_id" id="post_id" value={{ $post[0]->id }}>
        <textarea name="content" id="content" cols="65" rows="5"></textarea>
        <button onclick="comment()">Bình luận</button> &nbsp;<span style="color: red;font-size: 16px" id="errComment"></span>
     </div>
     @else
      <p style="color: red;font-size:14px">Vui lòng đăng nhập để có thể bình luận</p>
       @endif
       <h3 style="color: #837d7d">Bình luận</h3 style="color: #cdcdcd">
       @foreach ($dataComment as $item)
       @if($item->is_active===1)
       <div>
        <u>{{ $item->user->name }}</u>
        <p style="font-size: 14px">{{ $item->content }}</p>
        @if (Auth::user() && Auth::user()->id == $item->user_id)
               <button style="width: 40px; height: 30px;font-size: 12px;background: rgb(209, 155, 155);border:1px solid black" onclick="deleteComment({{ $item->id }})">Xóa</button>
               <button data-toggle="modal" data-target="#updateComment" style="width: 100px; height: 30px;font-size: 12px;background: rgb(241, 225, 225);border:1px solid black" type="button"onclick="editComment({{ $item->id }})" >Chỉnh sửa</button>
        @endif
        <p style="margin-left: 50%;font-size:14px">
          <i>{{ $post[0]->created_at }}</i>
         </p>
      </div>
       @endif
      <hr>
       @endforeach
       {{ $dataComment->links() }}
      </div>
    </div>
  </div>
</article>
<!-- Modal -->
<div class="modal fade" id="updateComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa bình luận</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('updateComment') }}" method="POST">
        @csrf
      <div class="modal-body">
          <div class="form-group">
            <input type="hidden" name='idComment' id="idComment">
            <label for="message-text" class="col-form-label">Nội dung:</label>
            <textarea class="form-control" id="message-text" name="contentComment"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
          {{-- <button type="submit" class="btn btn-primary" onclick="updateComment()">Chỉnh sửa</button> --}}
          <button type="submit"  class="btn btn-primary">Chỉnh sửa</button>
      </div>
    </form>
    </div>
  </div>
</div>
<script>
    comment = async () =>{
    let user_id = document.getElementById('user_id').value;
    let post_id = document.getElementById('post_id').value;
    let content = document.getElementById('content').value;
    if(content){
      const data = await axios.post('http://localhost:8000/postComment',{post_id,user_id,content});
      debugger
    if(!data.data.messages){
      window.location.reload();
      }else{
        document.getElementById('formComment').innerHTML = data.data.messages;
      }
    }else{
      document.getElementById('errComment').innerHTML = 'Vui lòng điền nội dung bình luận!';
    }
    
  }
  deleteComment = async (id) =>{
    const data = await axios.post(`http://localhost:8000/deleteComment/${id}`);
    if(data.data){
      Swal.fire({
          icon: 'success',
          title: data.data.messages,
          showConfirmButton: false,
          timer: 1500
        })
        setTimeout(() =>{
          window.location.reload()
        },2000)
    }
  }
  editComment = async id =>{
    const data = await axios.get(`http://localhost:8000/editComment/${id}`);
    if(data.data){
      document.getElementById('message-text').innerHTML = data.data.content;
      document.getElementById('idComment').value = data.data.id;
    }
  }
</script>
@endsection