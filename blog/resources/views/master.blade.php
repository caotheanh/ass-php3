<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <script src="{{asset('js/axios.min.js') }}"></script>
  <script src="{{asset('js/sweetalert2@9.js') }}"></script>
  <script src="{{asset('vendor/jquery/jquery.min.js') }}"></script>
  
  <!-- Custom styles for this template -->
  <link href="{{ asset('css/clean-blog.min.css') }}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="{{ route('home') }}">Cao Thế Anh</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">Trang chủ</a>
          </li>
          @if(Auth::user())
          <li class="nav-item">
            <a class="nav-link" href="{{ route('list-flw') }}">Danh sách người theo dõi</a>
          </li>
          @endif
          @if (!empty(Auth::user()))
          <li class="nav-item">
            <div class="dropdown">
              <a class="dropdown-toggle" style="color: white;margin-top: 3px" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               {{ Auth::user()->name }}
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                {{-- <a class="dropdown-item" href="#">Xem thông tin</a> --}}
                <a type="button" class="btn btn-default" data-toggle="modal" data-target="#profileData" data-whatever="@mdo" style="font-size: 14px">Xem thông tin</a>
                <a  class="dropdown-item" href="{{ route('profile') }}">Xem trang cá nhân</a>
                <a  class="dropdown-item" href="{{ route('logout') }}">Đăng xuất</a>
              </div>
            </div>
          </li>
          
          @else
          <li class="nav-item">
            <a type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Đăng nhập</a>
          </li>
          <li class="nav-item">
            <a type="button" class="btn btn-default" data-toggle="modal" data-target=".bd-example-modal-lg">Đăng ký</a>
          </li>
         
          @endif
          
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('{{asset('img/home-bg.jpg') }}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Cao Thế Anh</h1>
            <span class="subheading">Thế Anh PHP3</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  @yield('content')
  {{-- <form action="{{ route('register-user') }}" method="POST"> --}}
    {{-- @csrf --}}
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title " id="exampleModalLabel">Đăng ký tài khoản</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <span id="errRegister" style="color: red;margin:10px 0px 0px 10px"></span>
      <div class="modal-body">
        <div class="form-group">
          <label for="rName" class="col-form-label">Tên:</label>
          <input type="text" class="form-control" name="rName" id="rName">
        </div>
        <div class="form-group">
          <label for="rPhone" class="col-form-label">Số điện thoại:</label>
          <input type="text" class="form-control" name="rPhone" id="rPhone">
        </div>
        <div class="form-group">
          <label for="rDate" class="col-form-label">Ngày sinh</label>
          <input type="date" class="form-control" name="rDate" id="rDate">
        </div>
        <div class="form-group">
          <label for="rEmail" class="col-form-label">Email:</label>
          <input type="text" class="form-control" name="rEmail" id="rEmail">
        </div>
        <div class="form-group">
          <label for="rPassword" class="col-form-label">Mật khẩu:</label>
          <input type="password" class="form-control" name="rPassword" id="rPassword">
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
      <button onclick="register()" class="btn btn-primary">Đăng ký</button>
      {{-- <button type="submit" class="btn btn-primary">Đăng ký</button> --}}
    </div>
    </div>
  </div>
</div>
  </form>
  <form action="{{ route('login') }}" method="post">
    @csrf
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Đăng nhập</h5>
        <p class="err"></p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
        <div id="errLogin" style="color: red"></div>
          <div class="form-group">
            <label for="email" class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email" id="login-email">
          </div>
          <div class="form-group">
            <label for="password" class="col-form-label">Mật khẩu:</label>
            <input type="password" class="form-control" name="password" id="login-password">
          </div>
        {{-- </form> --}}
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
        <button type="submit" onclick="login()" class="btn btn-primary">Đăng nhập</button>
      </div>
    </div>
  </div>
</div>
</form>
@if (Auth::user())
<div class="modal fade" id="profileData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ Auth::user()->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="email" class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email" id="recipient-email" value="{{ Auth::user()->email  }}" disabled>
          </div>
          <div class="form-group">
            <label for="phone" class="col-form-label">Phone</label>
            <input type="number" class="form-control" name="phone" id="recipient-password" value="{{ Auth::user()->phone  }}" disabled>
          </div>
        {{-- </form> --}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
        <a type="button" class="btn btn-info" data-toggle="modal" data-target="#changePassword" data-dismiss="modal" data-whatever="@mdo">Đổi mật khẩu</a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ Auth::user()->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{-- <form action="{{ route('change-password') }}" method="POST"> --}}
        {{-- @csrf --}}
      <div class="modal-body">
        <p id="error-changePassword" style="color: red"></p>
          <div class="form-group">
            <label for="oldPassword" class="col-form-label">Mât khẩu cũ</label>
            <input type="password" class="form-control" name="oldPassword" id="oldPassword">
          </div>
          <div class="form-group">
            <label for="newPassword" class="col-form-label">Mật khẩu mới</label>
            <input type="password" class="form-control" name="newPassword" id="newPassword">
          </div>
          <div class="form-group">
            <label for="cfNewPassword" class="col-form-label">Xác nhận mật khẩu mới</label>
            <input type="password" class="form-control" name="cfNewPassword" id="cfNewPassword">
          </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
        <a onclick="changePassword()" class="btn btn-primary">Đổi mật khẩu</a>
      </div>
       {{-- </form> --}}
    </div>
  </div>
</div>
@endif
{{-- </form> --}}
   <!-- Footer -->
   <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  
  <!-- Bootstrap core JavaScript -->
  
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{asset('js/clean-blog.min.js') }}"></script>
    
  <script>
    login = async () =>{
      let email = document.getElementById('login-email').value;
      let password = document.getElementById('login-password').value;
      if(!email || !password){
        Swal.fire({
          icon: 'warning',
          title: 'Vui lòng điền đầy đủ trước khi bấm đăng nhập!',
          showConfirmButton: false,
          timer: 1500
        })
      }
    }
    changePassword = () =>{
      const x =location.href;
      debugger
      let oldPassword = document.getElementById('oldPassword').value;
      let newPassword = document.getElementById('newPassword').value;
      let cfNewPassword = document.getElementById('cfNewPassword').value;
      if(oldPassword && newPassword && cfNewPassword){
        const data = axios.post('http://localhost:8000/changePassword',{oldPassword,newPassword,cfNewPassword});
        if(data.data.messages){
          document.getElementById('error-changePassword').innerHTML = data.data.messages;
        }else{
          Swal.fire({
          icon: 'success',
          title: 'Đổi mật khẩu thành công!',
          showConfirmButton: false,
          timer: 2000
        })
        setTimeout(()=>{
          window.location.reload();
        },2000)
        }
      }else{
        document.getElementById('error-changePassword').innerHTML = 'Vui lòng điền đầy đủ thông tin!';
      }
    }
    register = () =>{
      let rName = document.getElementById('rName').value;
      let rPhone = document.getElementById('rPhone').value;
      let rDate = document.getElementById('rDate').value;
      let rEmail = document.getElementById('rEmail').value;
      let rPassword = document.getElementById('rPassword').value;
      if(rName && rPhone && rDate && rEmail && rPassword){
        let data = axios.post('http://localhost:8000/register',{rName,rPhone,rDate,rEmail,rPassword});
        if(!data.data.messages){
          Swal.fire({
          icon: 'success',
          title: 'Đăng ký thành công!',
          showConfirmButton: false,
          timer: 2000
        })
        setTimeout(() =>{
              window.location.reload();
            },3000)
        // const dataLogin = await axios.post('http://localhost:8000/login',{email:rEmail,password:rPassword});
        //   if(!dataLogin.data.messages){
        //     setTimeout(() =>{
        //       window.location.reload();
        //     },3000)
        //   }
        
        }else{
          document.getElementById('errRegister').innerHTML = data.data.messages;
        }
        
      }
    }
  </script>

</body>

</html>