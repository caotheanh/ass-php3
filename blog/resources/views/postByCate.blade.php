
@extends('master')
@section('content')
<div class="container">
 <div class="row">
   <div class="col-lg-3">
     <h3>Danh mục bài viết</h3>
     <ul class="list-group">
     @foreach ($cate as $item)
        <a href="{{ route('post-by-cate',['id'=>$item->id])}}"> <li class="list-group-item"> {{ $item->name }}</li></a>
     @endforeach
     </ul>
   </div>
   <div class="col-lg-9 col-md-10">
     @foreach ($post as $item)
     <div class="post-preview">
       <a href="{{ route('post-details',['id'=>$item->id]) }}">
         <h2 class="post-title">
           {{ $item->title }}
         </h2>
         <h3 class="post-subtitle">
           
             {{ substr($item->content,0,30).'....' }}
            
         </h3>
       </a>
       <p class="post-meta">Đăng bởi
         <a href="#">{{ $item->user->name }}</a>
         lúc {{ $item->created_at }}</p>
     </div>
     <hr>
     @endforeach
     <div class="clearfix">
       {{ $post->links() }}
     </div>
   </div>
 </div>
</div>

<hr>
@endsection



