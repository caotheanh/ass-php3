@extends('master') @section('content')
<div class="container">
    {{-- @if (count($data)>0) --}}

    <div>
        <span style="color: red"> {{ $user->name }}</span>
        {{-- <span style="color: red"> {{ $item->user->name }}</span> --}}
                    @if(Auth::user() && Auth::user()->id !=$user->id)
                    <form method="POST" action="{{ route('follow',['id' =>$user->id]) }}">
                        <input type="hidden" name="id_peopleFlow" value="{{$user->id }}">
                        @if(session('status'))
                        <button type="submit" disabled>{{ session('status') }}</button>
                        @elseif($messages=='Đang theo dõi')
                        <input type="button" value="{{ $messages }}" disabled />
                        @else
                        <button type="submit">Theo dõi</button>
                        @endif
                        
                    </form>
                    <form action="{{ route('unfollow',['id'=>$user->id]) }}" method="post">
                      @csrf
                        @if($messages=='Đang theo dõi')
                        <button type="submit">Hủy</button>
                        @endif
                    </form>
                    @endif
        @foreach ($data as $item)

        <div class="row">
            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                <div class="post-preview">
                    {{-- <span style="color: red"> {{ $item->user->name }}</span>
                    @if(Auth::user() && Auth::user()->id !=$item->user->id)
                    <form method="POST" action="{{ route('follow',['id' =>$item->user->id]) }}">
                        <input type="hidden" name="id_peopleFlow" value="{{ $item->user->id }}">
                        @if(session('status'))
                        <button type="submit">{{ session('status') }}</button>
                        @elseif($messages=='Đang theo dõi')
                        <input type="button" value="{{ $messages }}" disabled />
                        @else
                        <button type="submit">Theo dõi</button>
                        @endif
                        
                    </form>
                    @endif --}}
                    <a href="{{ route('post-details',['id'=>$item->id]) }}">
                        <h2 class="post-title">
                            {{ $item->title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ substr($item->content,0,30).'....' }}
                        </h3>
                    </a>
                    <p class="post-meta">
                        Đăng bởi
                        <a href="#">{{ $item->user->name }}</a>
                        lúc {{ $item->created_at }}
                    </p>
                </div>
            </div>
            <hr />
        </div>

        @endforeach
        <div class="modal fade" id="updatePost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa bài viết</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <input type="hidden" name='id' id="id">
                      <label for="title" class="col-form-label">Tiêu đề:</label>
                      <input class="form-control" id="title" name="title" />
                      <label for="content" class="col-form-label">Nội dung:</label>
                      <textarea class="form-control" id="content" name="content"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                    <button onclick="update()"  class="btn btn-primary">Chỉnh sửa</button>
                </div>
              {{-- </form> --}}
              </div>
            </div>
          </div>
        {{ $data->links() }}
    </div>

    {{-- @else
    <p>Người này chưa có bài viết nào !</p>
    @endif --}}
</div>
@endsection
