<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'AuthController@login');
Route::group([
    'prefix' => 'admin',
    'middleware' => 'jwt.auth'
], function () {
    Route::get('user-info', 'AuthController@getUserInfo');
    Route::group([
        'prefix' => 'categoris',
        'middleware' => 'checkPrermission'
    ], function () {
        Route::get('/', 'CategoryController@index');
        Route::post('/add', 'CategoryController@add');
        Route::get('edit/{id}', 'CategoryController@edit');
        Route::post('update', 'CategoryController@update');
        Route::post('destroy/{id}', 'CategoryController@destroy');
    });
    Route::group(['prefix' => 'posts'], function () {
        Route::get('/', 'PostController@index');
        Route::post('/add', 'PostController@add');
        Route::get('edit/{id}', 'PostController@edit');
        Route::post('update', 'PostController@update');
        Route::post('destroy/{id}', 'PostController@destroy');
        Route::post('unActiveComment/{id}','PostController@unActiveComment');
        Route::post('activeComment/{id}','PostController@activeComment');
    });
    Route::group(['prefix' => 'comment'], function (){
        Route::get('numberComment/{id}','AdminCommentController@getCount');
        Route::get('commentByPost/{id}','AdminCommentController@commentByPost');
        Route::post('activeComment/{id}','AdminCommentController@activeComment');
        Route::get('detailsComment/{id}','AdminCommentController@detailsComment');
    });
    Route::group(['prefix' => 'user'], function(){
        Route::get('listUser','UserController@index');
        Route::post('blockUser/{id}','UserController@blockUser');
    });
});

// Route::group(['middleware' => 'jwt.auth'], function () {
//     Route::get('user-info', 'AuthController@getUserInfo');
// });
