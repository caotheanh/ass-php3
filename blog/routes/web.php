<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('post/{id}', 'HomeController@details')->name('post-details');
Route::get('editPost/{id}', 'HomeController@editPost')->name('edit-post');
Route::post('savePost', 'HomeController@savePost')->name('savePost');
Route::post('postComment', 'CommentController@comment')->name('post-comment');
Route::get('postbyCate/{id}','HomeController@postByCate')->name('post-by-cate');
Route::post('login', 'HomeController@login')->name('login');
Route::get('logout', 'HomeController@logout')->name('logout');
Route::post('register','HomeController@registerUser')->name('register-user');
Route::post('changePassword', 'HomeController@changePassword')->name('change-password');
Route::get('profile', 'HomeController@profile')->name('profile');
Route::post('profile', 'HomeController@postProfile')->name('profile-post');
Route::post('deleteProfilePost/{id}', 'HomeController@deleteProfilePost')->name('deleteProfilePost');
Route::post('deleteComment/{id}','CommentController@deleteComment')->name('deleteComment');
Route::get('editComment/{id}','CommentController@editComment')->name('editComment');
Route::post('updateComment','CommentController@updateComment')->name('updateComment');
Route::get('/profile-follow/{id}','HomeController@profile_follow')->name('profile-follow');
Route::post('/follow/{id}','HomeController@follow')->name('follow');
Route::post('/unfollow/{id}','HomeController@unfollow')->name('unfollow');
Route::get('list-flw','HomeController@listFlw')->name('list-flw');
