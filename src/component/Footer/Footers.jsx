import React, { Component } from 'react';
import { Layout} from "antd";
const { Footer } = Layout;
class Footers extends Component {
    render() {
        return (
            <Footer style={{ textAlign: "center" }}>
            Lập trình PHP3 - Cao Thế Anh
          </Footer>
        );
    }
}

export default Footers;