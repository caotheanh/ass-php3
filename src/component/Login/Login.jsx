import React, { Component } from 'react';
import 'antd/dist/antd.css';
import axios from 'axios';
import swal from 'sweetalert';
import { api } from '../../api';
import { Form, Input, Button, Checkbox, Row, Col } from 'antd';
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: true
        }
    }
    render() {
        const onFinish = async values => {
            try {
                const data = await axios.post(`${api}/auth/login`, values);
                if (data.data.token) {
                    swal("Đăng nhập thành công!");
                    localStorage.setItem('token', `${data.data.token}`)
                    window.location.href = "/"
                } else {
                    swal("Đăng nhập thất bại!");
                    localStorage.removeItem('token');
                }
            } catch (e) {
                swal("Đăng nhập thất bại!");
                localStorage.removeItem('token');
            }
        };

        const onFinishFailed = errorInfo => {
            console.log('Failed:', errorInfo);
        };
        return (
            <>
                <Row style={{ marginTop: '50px' }}>
                    <Col span={12} offset={4}>
                        <h2>Login</h2>
                        <Form
                            {...layout}
                            name="basic"
                            initialValues={{
                                remember: false,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your email!',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your password!',
                                    },
                                ]}
                            >
                                <Input.Password />
                            </Form.Item>

                            <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                                <Checkbox>Remember me</Checkbox>
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Login
                                    </Button>
                            </Form.Item>
                        </Form>
                    </Col>
                </Row>

            </>

        );
    }
}

export default Login;