import React, { Component } from 'react';
import axios from 'axios';
import { Table, Modal, Button } from 'antd';
import { api } from '../../api';
import swal from 'sweetalert';
class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: [],
            selectedRows: [],
            dataCommentPost: [],
            redirect: false,
            // visible:false
        }
    }
    // showModal = () =>{
    //     this.setState({visible:true})
    // }
    // handleOk = (e) =>{
    //     this.setState({visible:false})
    // }
    componentDidMount = async () => {
        const commentPostId = this.props.match.params.key;
        let data = [];
        const dataComment = await axios.get(`${api}/admin/comment/commentByPost/${commentPostId}?token=${localStorage.getItem('token')}`);
        if (dataComment.data) {
            debugger
            for (let i = 0; i < dataComment.data.length; i++) {
                data.push({
                    key: dataComment.data[i].id,
                    content: dataComment.data[i].content,
                    user_id: dataComment.data[i].user.name,
                    updated_at: dataComment.data[i].updated_at,
                    status:dataComment.data[i].is_active===1?'Hiện':'Ẩn'
                })

            }
            await this.setState({ dataCommentPost: data });
        }
    }
    onSelectChange = async (selectedRowKeys, selectedRows) => {
        this.setState({ selectedRowKeys, selectedRows });
        if (selectedRows.length > 0) {
            this.setState({ redirect: true })
        } else {
            this.setState({ redirect: false })
        }
    };
    hiddenComment = async id =>{
        const data = await axios.post(`${api}/admin/comment/activeComment/${id}?token=${localStorage.getItem('token')}`);
        if(!data.data.messages){
            swal("Bình luận này đã bị ẩn!", {
                icon: "success",
            });
            setTimeout(()=>{
                window.location.reload();
            },1500)
        }else{
            swal(data.data.messages, {
                icon: "warning",
            });
        }
    }
    info =async (id) =>{
        const data = await axios.get(`${api}/admin/comment/detailsComment/${id}?token=${localStorage.getItem('token')}`)
        if(data.data){
            Modal.info({
                title:'Chi tiết bình luận',
                content:(
                    <div>
                        {data.data.content}
                    </div>
                ),
                onOk(){}
            })
        }
        
    }
    render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'Nội dung bình luận',
                dataIndex: 'content',
                key: 'content',
            },
            {
                title: 'Người bình luận',
                dataIndex: 'user_id',
                key: 'user_id',
            },
            {
                title: 'Thời gian',
                dataIndex: 'updated_at',
                key: 'updated_at',
            },
            {
                title: 'Trạng thái',
                dataIndex: 'status',
                key: 'status',
            },
        ];
        return (
            <div>
                {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ?
                    <Button type='danger' style={{ margin:'0px 10px 10px 0px' }}
                    onClick= {() => this.info(this.state.selectedRows[0].key)}
                    >
                        Xem chi tiết
                    </Button> : ''}
                 {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ?
                    <Button type='primary' style={{ margin:'0px 10px 10px 0px' }}
                    onClick= {() =>this.hiddenComment(this.state.selectedRows[0].key)}
                    >
                        Ẩn bình luận
                    </Button> : ''}
                <Table rowSelection={rowSelection} columns={columns} dataSource={this.state.dataCommentPost} />
            </div>
        );
    }
}

export default List;