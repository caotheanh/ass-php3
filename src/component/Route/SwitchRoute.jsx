import React, { Component } from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
import Home from '../Content/Home';
import Add from '../Post/Add';
import HomeCate from '../Category/HomeCate';
import CategoryAdd from '../Category/CategoryAdd';
import CategoryEdit from '../Category/CategoryEdit';
import List from '../Comment/List';
import ListUser from '../User/ListUser';
class SwitchRoute extends Component {
    render() {
        return (
            <Switch>
                <Route exact path={'/'} component={Home} />
                <Route  path={'/addPost'} component={Add} />
                <Route  path={'/category'} component={HomeCate} />
                <Route  path={'/addCategory'} component={CategoryAdd} />
                <Route  path={'/editCategory/:key'} component={CategoryEdit} />
                <Route  path={'/commentByPost/:key'} component={List} />
                <Route  path={'/user'} component={ListUser} />
                <Redirect to='/' />
            </Switch>
        );
    }
}

export default SwitchRoute;