import React, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'antd';
import { api } from '../../api';
import swal from 'sweetalert';
class ListUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: [],
            selectedRows: [],
            dataUser: [],
            redirect: false,
        }
    }
    componentDidMount = async () => {
        let data = [];
        const user = await axios.get(`${api}/admin/user/listUser?token=${localStorage.getItem('token')}`);
        if (user.data) {
            for (let i = 0; i < user.data.length; i++) {
                data.push({
                    key: user.data[i].id,
                    name: user.data[i].name,
                    DateOfBirth: user.data[i].DateOfBirth,
                    phone: user.data[i].phone,
                    email: user.data[i].email,
                    permission:user.data[i].role===1?'Người dùng':'Admin'
                })

            }
            await this.setState({ dataUser: data });
        }
    }
    onSelectChange = async (selectedRowKeys, selectedRows) => {
        this.setState({ selectedRowKeys, selectedRows });
        if (selectedRows.length > 0) {
            this.setState({ redirect: true })
        } else {
            this.setState({ redirect: false })
        }
    };
    blockUser = async id =>{
        const data = await axios.post(`${api}/admin/user/blockUser/${id}&token=${localStorage.getItem('token')}`);
        if(!data.data.messages){
            swal(`Đã khóa tài khoản`, {
                icon: "success",
            });
        }else{
            swal(`Tài khoản này đã bị khóa trước đó!`, {
                icon: "warning",
            });
        }
    }
    render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'Tên',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Ngày sinh',
                dataIndex: 'DateOfBirth',
                key: 'DateOfBirth',
            },
            {
                title: 'Số điện thoại',
                dataIndex: 'phone',
                key: 'phone',
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'Quyền',
                dataIndex: 'permission',
                key: 'permission',
            },
        ];
        return (
            <div>
                 {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ?
                    <Button type='primary' style={{ margin:'0px 10px 10px 0px' }}
                    onClick= {() =>this.blockUser(this.state.selectedRows[0].key)}
                    >
                        Khóa tài khoản
                    </Button> : ''}
                <Table rowSelection={rowSelection} columns={columns} dataSource={this.state.dataUser} />
            </div>
        );
    }
}

export default ListUser;