import React, { Component } from 'react';
import { Table, Button } from 'antd';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import { api } from '../../api';
import axios from 'axios';
// import axios from '../../axiosConfig'

let dataPost = [];
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: [],
            selectedRows: [],
            data: [],
            redirect: false,
        };
    }
    componentDidMount = async () => {
        // const dataaaa = await axios.get(`${api}/checkAuth`);
        // console.log(dataaaa);
        // debugger
        dataPost = await axios.get(`${api}/admin/posts?token=${localStorage.getItem('token')}`);
        let data = [];
        for (let i = 0; i < dataPost.data.length; i++) {
            let idPost = dataPost.data[i].id;
            const countComment = await axios.get(`http://localhost:8000/api/admin/comment/numberComment/${idPost}?token=${localStorage.getItem('token')}`)
            data.push({
                key: dataPost.data[i].id,
                title: dataPost.data[i].title,
                content: dataPost.data[i].content,
                cate_id: dataPost.data[i].cate.name,
                user_id: dataPost.data[i].user.name,
                count_comment: countComment.data,
                // status_comment: dataPost.data[i].is_comment === 1 ? 'Cho phép' : 'Chặn'
            })
        }

        await this.setState({ data: data })


    }
    onSelectChange = async (selectedRowKeys, selectedRows) => {
        this.setState({ selectedRowKeys, selectedRows });
        if (selectedRows.length > 0) {
            this.setState({ redirect: true })
        } else {
            this.setState({ redirect: false })
        }

    };
    onDelete = () => {
        if (this.state.selectedRowKeys.length === 0 || this.state.selectedRowKeys.length > 1) {
            swal("Vui lòng chọn 1 mục cần xóa!");
        } else {
            swal({
                title: "Bạn có chắc chắn muốn xóa",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        const id = this.state.selectedRows[0].key;
                        axios.post(`${api}/admin/posts/destroy/${id}?token=${localStorage.getItem('token')}`).then(res => {
                            if (res.data) {
                                const data = this.state.data.filter(el => el.key !== id);
                                this.setState({ data: data });
                                swal("Xóa thành công!", {
                                    icon: "success",
                                });
                                this.setState({ selectedRowKeys: [] });
                                this.setState({ selectedRows: [] });
                            } else {
                                swal("Xóa thất bại!", {
                                    icon: "warning",
                                });
                            }
                        });
                    } else {
                        this.setState({ selectedRowKeys: [] });
                        this.setState({ selectedRows: [] });
                    }
                });
        }
    }
    un_activeComment = async id => {
        const statusComment = await axios.get(`${api}/admin/posts/edit/${id}?token=${localStorage.getItem('token')}`);
        console.log(statusComment.data[0].is_comment);

        debugger
        if (statusComment.data[0].is_comment === 1) {
            const data = await axios.post(`${api}/admin/posts/unActiveComment/${id}?token=${localStorage.getItem('token')}`);
            if (!data.data.messages) {
                swal("Đã tắt bình luận", {
                    icon: "success",
                });
                this.setState({ selectedRowKeys: [] });
                this.setState({ selectedRows: [] });
                // setTimeout(()=>{
                //     this.props.history.push('/');
                // },2000)
            }
        } else {
            swal("Bài viết này đã bị tắt bình luận!", {
                icon: "warning",
            });
            this.setState({ selectedRowKeys: [] });
            this.setState({ selectedRows: [] });
        }

    }
    activeComment = async id => {
        const statusComment = await axios.get(`${api}/admin/posts/edit/${id}?token=${localStorage.getItem('token')}`);
        if (statusComment.data[0].is_comment === 2) {
            const data = await axios.post(`${api}/admin/posts/activeComment/${id}?token=${localStorage.getItem('token')}`);
            // debugger
            if (!data.data.messages) {
                swal("Đã mở bình luận", {
                    icon: "success",
                });
                // setTimeout(()=>{
                //     this.props.history.push('/');
                // },2000)
                this.setState({ selectedRowKeys: [] });
                this.setState({ selectedRows: [] });
            }
        } else {
            swal("Bài viết này không bị tắt bình luận!", {
                icon: "warning",
            });
            this.setState({ selectedRowKeys: [] });
            this.setState({ selectedRows: [] });
        }

    }
    render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'Tiêu đề',
                dataIndex: 'title',
                key: 'title',
                width: '20%',
            },
            {
                title: 'Nội dung',
                dataIndex: 'content',
                key: 'content',
                width: '20%',
            },
            {
                title: 'Danh mục',
                dataIndex: 'cate_id',
                key: 'cate_id',
            },
            {
                title: 'Người tạo',
                dataIndex: 'user_id',
                key: 'user_id',
            },
            {
                title: 'Số bình luận',
                dataIndex: 'count_comment',
                key: 'count_comment',
            },
            // {
            //     title: 'Trạng thái bình luận',
            //     dataIndex: 'status_comment',
            //     key: 'status_comment',
            // },
        ];

        return (
            <>
                <Link style={{ margin: '0px 10px 10px 0px', background: '#00CC00', color: 'white', padding: '7px' }} to='/addPost' >
                    Thêm mới bài viết
                </Link>
                <Button type='primary' onClick={() => this.onDelete()} style={{ marginBottom: '10px' }}>
                    Xóa
                </Button>
                {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ?
                    <Link style={{ margin: '0px 0px 10px 10px', background: '#006633', color: 'white', padding: '7px' }}
                        to={`/commentByPost/${this.state.selectedRows[0].key}`}>
                        Xem danh sách bình luận
                     </Link> : ''}
                {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ?
                    <Button style={{ margin: '0px 0px 10px 10px', background: '#006633', color: 'white' }}
                        onClick={() => this.un_activeComment(this.state.selectedRows[0].key)}
                    >
                        Tắt bình luận
                     </Button> : ''}
                {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ?
                    <Button style={{ margin: '0px 0px 10px 10px', background: '#006633', color: 'white' }}
                        onClick={() => this.activeComment(this.state.selectedRows[0].key)}
                    >
                        Bật bình luận
                     </Button> : ''}
                <Table rowSelection={rowSelection} columns={columns} dataSource={this.state.data} />
            </>
        );
    }
}

export default Home;