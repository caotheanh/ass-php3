import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col, Select } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { Link } from 'react-router-dom';
import { api } from '../../api';
import axios from 'axios';
import swal from 'sweetalert';
const { Option } = Select;
function onSearch(val) {
    console.log('search:', val);
}
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};
class Add extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: true,
            cate_id: [],
            err_cate: '',
            cate: ''
        }
    }
    async componentDidMount() {
        const category = await axios.get(`${api}/admin/categoris?token=${localStorage.getItem('token')}`);
        await this.setState({ cate_id: category.data });
    }
    onChange = async (value) => {
        if (value) {
            await this.setState({ cate: value })
        } else {
            await this.setState({ cate: '' })
        }
    }
    onFinish = async values => {
        if (!this.state.cate) {
            await this.setState({ err_cate: 'Không bỏ trống danh mục!' })
        } else {
            await this.setState({ err_cate: '' })
            values.user_id = 1;
            values.cate_id = this.state.cate;
            values.is_comment = 1;
            const result = await axios.post(`${api}/admin/posts/add?token=${localStorage.getItem('token')}`, values);
            if (result.data) {
                swal("Tạo mới thành công!");
                setTimeout(() => {
                    this.props.history.push('/');
                }, 2000)
            }

        }
    };
    render() {
        const onFinishFailed = errorInfo => {
            console.log('Failed:', errorInfo);
        };
        return (
            <>
                <Row>
                    <Col span={16}>
                        <h2 style={{ textAlign: 'center' }}>Thêm mới bài viết</h2>
                        <Form
                            {...layout}

                            initialValues={{
                                remember: false,
                            }}
                            onFinish={this.onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Tiêu đề"
                                name="title"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Không bỏ trống tiêu đề!',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Nội dung"
                                name="content"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Không bỏ trống nội dung!',
                                    },
                                ]}
                            >
                                <TextArea style={{ height: '200px' }} />
                            </Form.Item>
                            <Form.Item {...layout}
                                label='Danh mục'
                                name='cate_id'
                            >
                                <Select
                                    showSearch
                                    style={{ width: 200 }}
                                    placeholder="Select a person"
                                    optionFilterProp="children"
                                    onChange={this.onChange}
                                    onSearch={onSearch}
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {this.state.cate_id.map(el => (<Option value={el.id}>{el.name}</Option>))}
                                </Select> {this.state.err_cate ? <span style={{ color: 'red' }}>{this.state.err_cate}</span> : ''}
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Thêm mới bài viết
                                    </Button>
                                <Link type="primary" to='/'>
                                    Quay lại
                                    </Link>
                            </Form.Item>

                        </Form>
                    </Col>
                </Row>

            </>

        );
    }
}

export default Add;