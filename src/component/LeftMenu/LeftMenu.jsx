import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
    CloudOutlined,
    UserOutlined,
    UploadOutlined,
    VideoCameraOutlined,
} from '@ant-design/icons';
import { Layout, Menu } from 'antd';


const { Sider } = Layout;

class LeftMenu extends Component {
    render() {
        return (
            <Sider
                style={{
                    overflow: 'auto',
                    height: '100vh',
                    position: 'fixed',
                    left: 0,
                }}
            >
                <div className="logo" />
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                    <Menu.Item  icon={<UserOutlined />} style={{ height: '50px' }}>
                        <Link to='/'>Blog Thế Anh</Link>
                    </Menu.Item>
                    <Menu.Item  icon={<CloudOutlined />}>
                        <Link to='/post'>Quản trị bài viết</Link>
                    </Menu.Item>
                    <Menu.Item  icon={<VideoCameraOutlined />}>
                        <Link to='/user'>Quản trị người dùng</Link>
                    </Menu.Item>
                    <Menu.Item  icon={<UploadOutlined />}>
                        <Link to='/category'>Quản trị danh mục</Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        );
    }
}

export default LeftMenu;