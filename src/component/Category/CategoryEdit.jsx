import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Button, Input, Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { api } from '../../api';
import swal from 'sweetalert';
class CategoryEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: true,
            name: '',
            err: '',
            id: ''
        }
    }
    async componentDidMount() {
        const id = this.props.match.params.key;
        debugger
        const data = await axios.get(`${api}/admin/categoris/edit/${id}?token=${localStorage.getItem('token')}`);
        
        await this.setState({ name: data.data.name })
        await this.setState({ id: data.data.id })

    }
    changeInput = async event => {
        let { value } = event.target;
        await this.setState({
            ...this.state,
            [event.target.name]: value
        })
        console.log(this.state.id);
        
    }
    onFinish = async () => {
        if (!this.state.name) {
            await this.setState({ err: 'Không được để trống tên danh mục' });
        } else {
            const data = {
                id: this.state.id,
                name: this.state.name,
            }
            const updatePost = await axios.post(`${api}/admin/categoris/update?token=${localStorage.getItem('token')}`,data);
            console.log(updatePost);
            debugger
            if(updatePost.data){
                swal("Cập nhật thành công!");
               setTimeout(() =>{
                this.props.history.push('/category');
               },2000)
               await this.setState({ err: '' })
            }else{
                swal("Cập nhật thất bại!");
            }
            
        }
    };
    render() {

        return (
            <>
                <Row>
                    <Col span={16} offset={3}>

                        <h2 style={{ textAlign: 'center' }}>Chỉnh sửa danh mục</h2>
                        <input type='hidden' name='id' value={this.state.id} />
                        {this.state.err ? <p style={{ color: 'red' }}>{this.state.err}*</p> : ''}
                        <label htmlFor="name">Tên danh mục*</label>
                        <Input type='text' onChange={this.changeInput} name='name' value={this.state.name} />
                        <div style={{ marginTop: '20px' }}>
                            <Button type="primary" onClick={this.onFinish}>
                                Chỉnh sửa danh mục
                                    </Button>
                            <Link type="primary" to='/category'>
                                Quay lại
                                    </Link>
                        </div>
                    </Col>
                </Row>

            </>

        );
    }
}

export default CategoryEdit;