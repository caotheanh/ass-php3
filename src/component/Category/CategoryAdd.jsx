import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { api } from '../../api';
import swal from 'sweetalert';
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};
class CategoryAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: true,
        }
    }
    render() {
        const onFinish = async values => {
            values.user_id=1;
            const result = await axios.post(`${api}/admin/categoris/add?token=${localStorage.getItem('token')}`,values);
            if(result.data){
                swal("Tạo mới thành công!");
                setTimeout(() =>{
                    this.props.history.push('/category');
                   },2000)
            }else{
                swal("Tạo mới thất bại!");
            }

        };

        const onFinishFailed = errorInfo => {
            console.log('Failed:', errorInfo);
        };
        return (
            <>
                <Row>
                    <Col span={16}>
                        <h2 style={{ textAlign: 'center' }}>Thêm mới danh mục</h2>
                        <Form
                            {...layout}

                            initialValues={{
                                remember: false,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="name"
                                name="name"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Không bỏ trống!',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Thêm mới danh mục
                                    </Button>
                                    <Link type="primary" to='/category'>
                                    Quay lại
                                    </Link>
                            </Form.Item>

                        </Form>
                    </Col>
                </Row>

            </>

        );
    }
}

export default CategoryAdd;