import React, { Component } from 'react';
import { Table, Input, Button } from 'antd';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { api } from '../../api';
import swal from 'sweetalert';
// const { Search } = Input;
// const dataSearch = [];
class HomeCate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: [],
            selectedRows: [],
            dataCate: [],
            redirect: false,
            dataSearch: ''
        };
    }

    componentDidMount = async () => {
        let data = [];
        const category = await axios.get(`${api}/admin/categoris?token=${localStorage.getItem('token')}`);
        category.data.forEach(el => {
            data.push({
                key: el.id,
                name: el.name,
                user_id: el.user.name
            });
        })
        await this.setState({ dataCate: data })

    }
    onSelectChange = async (selectedRowKeys, selectedRows) => {
        this.setState({ selectedRowKeys, selectedRows });
        if (selectedRows.length > 0) {
            this.setState({ redirect: true })
        } else {
            this.setState({ redirect: false })
        }

    };
    onDelete = () => {
        if (this.state.selectedRowKeys.length === 0 || this.state.selectedRowKeys.length > 1) {
            swal("Vui lòng chọn 1 mục cần xóa!");
        } else {
            swal({
                title: "Bạn có chắc chắn muốn xóa",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then(async (willDelete) => {
                    if (willDelete) {
                        const id = this.state.selectedRows[0].key;
                        const deleteData = await axios.post(`${api}/admin/categoris/destroy/${id}?token=${localStorage.getItem('token')}`);
                        if (deleteData.data) {
                            const data = this.state.dataCate.filter(el => el.key !== id);
                            this.setState({ dataCate: data });
                            swal("Xóa thành công!", {
                                icon: "success",
                            });
                            this.setState({ selectedRowKeys: [] });
                            this.setState({ selectedRows: [] });
                        } else {
                            swal("Xóa thất bại!");
                        }

                    } else {
                        this.setState({ selectedRowKeys: [] });
                        this.setState({ selectedRows: [] });
                    }
                });
        }
    }
    changeSearch = async event => {
        let { value } = event.target;
        await this.setState({
            ...this.state,
            [event.target.name]: value
        })
        if (this.state.dataSearch === '') {
            let data = [];
            const category = await axios.get(`${api}/admin/categoris?token=${localStorage.getItem('token')}`);
            category.data.forEach(el => {
                data.push({
                    key: el.id,
                    name: el.name,
                    user_id: el.user.name
                });
            })
            await this.setState({ dataCate: data })
        }
    }
    onSearchData = async event => {

        if (this.state.dataSearch) {
            let result = this.state.dataCate.filter(el => String(el.name)
                .toLowerCase() //k phân biệt hoa thường
                .indexOf(String(this.state.dataSearch).toLowerCase()) > -1)
            if (result) {
                this.setState({ dataCate: result })
            }
        } else if (this.state.dataSearch === '') {
            let data = [];
            const category = await axios.get(`${api}/admin/categoris?token=${localStorage.getItem('token')}`);
            category.data.forEach(el => {
                data.push({
                    key: el.id,
                    name: el.name,
                    user_id: el.user.name
                });
            })
            await this.setState({ dataCate: data })
        }

    }
    render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: 'Người tạo',
                dataIndex: 'user_id',
                key: 'user_id'
            },

        ];
        return (
            <>
                {/* <Search style={{ marginBottom: '20px' }} placeholder="Tìm kiếm danh mục" onChange={this.onSearchData} onSearch={this.onSearchData} enterButton /> */}
                <Input name='dataSearch' value={this.state.dataSearch} placeholder='Tìm kiếm danh mục' onChange={this.changeSearch} style={{ marginBottom: '20px', width: '70%' }} />
                <Button danger onClick={this.onSearchData}>Tìm kiếm</Button> <br />
                <Link style={{ margin: '0px 10px 10px 0px', background: '#00CC00', color: 'white', padding: '7px' }} to='/addCategory' >Thêm mới danh mục</Link>
                <Button type='primary' onClick={() => this.onDelete()} style={{ marginBottom: '10px' }}>Xóa</Button>
                {this.state.selectedRows.length === 1 && this.state.selectedRows.length > 0 && this.state.redirect ? <Link style={{ margin: '0px 0px 10px 10px', background: '#006633', color: 'white', padding: '7px' }} to={`/editCategory/${this.state.selectedRows[0].key}`}>Cập nhật</Link> : ''}
                <Table rowSelection={rowSelection} columns={columns} dataSource={this.state.dataCate} />
            </>
        );
    }
}

export default HomeCate;