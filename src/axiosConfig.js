import axios from "axios";

var init = axios.create({
  baseURL: "http://localhost:8000/api/admin",
  timeout: 30000,
  headers: {
    Authorization: `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU5MjQ4MDAzMCwiZXhwIjoxNTkyNDgzNjMwLCJuYmYiOjE1OTI0ODAwMzAsImp0aSI6Ik11RVdtcFhpVDE0djFZeEMiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.WbP1Bc09wp7Bkux-q-sQN8tgcxhNJ1kQFRTBe15kifQ`,
  },
});

export default init;