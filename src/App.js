import React, { Component } from "react";
import axios from "axios";
import "antd/dist/antd.css";
import "./index.css";
import { Layout, Dropdown, Button, Menu } from "antd";
import { DownOutlined } from "@ant-design/icons";
import LeftMenu from "./component/LeftMenu/LeftMenu";
import Footers from "./component/Footer/Footers";
import SwitchRoute from "./component/Route/SwitchRoute";
import Login from "./component/Login/Login";
import { api } from "./api";
const { Header, Content } = Layout;
const logout = async () => {
  await axios.post(`${api}/logout`);
  localStorage.setItem("email", "");
  localStorage.setItem("role", "");
  window.location.reload();
};
const menu = (
  <Menu>
    <Menu.Item key="1" onClick={() => {localStorage.removeItem('token');window.location.reload()}}>
      Đăng xuất
    </Menu.Item>
  </Menu>
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: false,
      email:''
    };
  }
  async componentDidMount() {
    const user = await axios.get(
      `${api}/admin/user-info?token=${localStorage.getItem("token")}`
    );
    if (user.data.result) {
      localStorage.setItem("email", user.data.result.email);
      await this.setState({ status: true });
    } 
  }
  render() {
    return (
      <>
        {this.state.status ? (
        <Layout>
          <LeftMenu />
          <Layout className="site-layout" style={{ marginLeft: 200 }}>
            <Header className="site-layout-background" style={{ padding: 0 }}>
              <Dropdown overlay={menu}>
                <Button
                  type="primary"
                  style={{ float: "right", margin: "20px" }}
                >
                  {localStorage.getItem("email")} <DownOutlined />
                </Button>
              </Dropdown>
            </Header>

            <Content style={{ margin: "24px 16px 0", overflow: "initial" }}>
              <div
                className="site-layout-background"
                style={{ padding: 24, background: "white", height: "100vh" }}
              >
                <SwitchRoute />
              </div>
            </Content>
            <Footers />
          </Layout>
        </Layout>
         ) : (
         
          <Login />
        )} 
      </>
    );
  }
}

export default App;
